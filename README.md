# Public transport to Swiss Mountains - Openstack infrastructure

See the [Travel destination finder readme](https://gitlab.com/public-transport-to-swiss-mountains/destination-finder/-/blob/main/README.md) for general project information.

## Openstack infrastructure
This respository contains cloud deployment information for the [Travel planer](https://gitlab.com/public-transport-to-swiss-mountains/travel-planner). The travel planer needs to be executed periodicly in order to update the destinations and corresponding schedules list according to the most recent public transport timetables and stops list. The infrastructure used is based on an [Openstack](https://www.openstack.org/) infrastructure solution.

## Requirements
* Openstack project created with corresponding `openstack_config.txt` according to the example `openstack_config.txt.example`.
* OpenStack Client installed
* Swift Client installed
* Heat Client installed

## Usage
### Deploy persistent object storage
A persistent object storage is used to store generated travel lists.

```
source openstack_config.txt
openstack container create public-transport-swiss-mountains
swift post -r '.r:*,.rlistings' public-transport-swiss-mountains
```

Setup access object via Swift:
```
swift auth > swift_auth.txt
source swift_auth.txt
```

### Setup automatic deployment with Gitlab Pipelines
#### Creating SSH-keypair
```
openstack keypair create gitlab_cicd > ~/.ssh/gitlab_cicd
chmod 600 ~/.ssh/gitlab_cicd
```

#### Manual instance deployment
```
source openstack_config.txt
source swift_auth.txt
openstack stack create --template travel_planer.yml \
    --parameter openstack_storage_url=$OS_STORAGE_URL \
    --parameter openstack_auth_token=$OS_AUTH_TOKEN \
    --parameter start_stations=$START_STATIONS \
    --parameter exolabs_base_url=$EXOLABS_BASE_URL \
    --parameter exolabs_api_key=$EXOLABS_API_KEY \
    --wait travel_planer
```

## License
See [license file](https://gitlab.com/public-transport-to-swiss-mountains/travel-planner/-/blob/main/LICENSE?ref_type=heads).