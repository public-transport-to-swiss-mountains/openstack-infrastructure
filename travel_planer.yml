heat_template_version: 2018-08-31

description: Travel planer calculation instance

parameters:
  instance_name:
    type: string
    description: VM Name
    default: travel_planer_d01
  key_name:
    type: string
    label: Key Name
    description: Key-pair for VM access
    default: gitlab_cicd
  flavor_name:
    type: string
    description: VM Flavor
    default: a16-ram64-disk50-perf1
  image:
    type: string
    description: VM Image
    default: Debian 12 bookworm
  openstack_storage_url:
    type: string
    description: Openstack object storage URL
    hidden: true
  openstack_auth_token:
    type: string
    description: Openstack object storage auth token
    hidden: true
  openstack_container_name:
    type: string
    description: Openstack object storage container name
    default: public-transport-swiss-mountains
  start_stations:
    type: string
    description: Start stations
    default: Bern
  exolabs_base_url:
    type: string
    description: Exolabs API base URL
    default: https://cosmos-project.ch
  exolabs_api_key:
    type: string
    description: Exolabs API key
    hidden: true

resources:
  wait_condition:
    type: OS::Heat::WaitCondition
    properties:
      handle: { get_resource: wait_handle }
      timeout: 7200

  wait_handle:
    type: OS::Heat::WaitConditionHandle

  server:
    type: OS::Nova::Server
    properties:
      name: { get_param: instance_name }
      key_name: { get_param: key_name }
      image: { get_param: image }
      flavor: { get_param: flavor_name }
      networks: [ "network": ext-net1 ]
      user_data:
        str_replace:
          template: |
            #!/bin/bash -v
            sudo apt-get update
            sudo apt-get install ca-certificates curl
            sudo install -m 0755 -d /etc/apt/keyrings
            sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
            sudo chmod a+r /etc/apt/keyrings/docker.asc
            echo \
              "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
              $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
              sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
            sudo apt-get update
            sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

            git clone https://gitlab.com/public-transport-to-swiss-mountains/travel-planner.git
            cd travel-planner
            echo "ROOT_DATA=./tmp" >> .env
            echo "START_STATIONS=PARAM_START_STATIONS" >> .env
            echo "EXOLABS_BASE_URL=PARAM_EXOLABS_BASE_URL" >> .env
            echo "EXOLABS_API_KEY=PARAM_EXOLABS_API_KEY" >> .env
            sudo ./generate_schedules.sh 1000

            OS_STORAGE_URL="OPENSTACK_STORAGE_URL"
            OS_AUTH_TOKEN="OPENSTACK_AUTH_TOKEN"
            curl -i -T ./tmp/data/stops/traveltimes.csv -X PUT -H "X-Auth-Token: $OS_AUTH_TOKEN" $OS_STORAGE_URL/OPENSTACK_CONTAINER_NAME/traveltimes.csv
            curl -i -T ./tmp/data/stops/stops.csv -X PUT -H "X-Auth-Token: $OS_AUTH_TOKEN" $OS_STORAGE_URL/OPENSTACK_CONTAINER_NAME/stops.csv
            curl -i -T ./tmp/data/stops/snow_coverage.csv -X PUT -H "X-Auth-Token: $OS_AUTH_TOKEN" $OS_STORAGE_URL/OPENSTACK_CONTAINER_NAME/snow_coverage.csv

            WC_NOTIFY --data-binary '{"status": "SUCCESS"}'
          
          params:
            WC_NOTIFY: { get_attr: ['wait_handle', 'curl_cli'] }
            OPENSTACK_STORAGE_URL: { get_param: openstack_storage_url }
            OPENSTACK_AUTH_TOKEN: { get_param: openstack_auth_token }
            OPENSTACK_CONTAINER_NAME: { get_param: openstack_container_name }
            PARAM_START_STATIONS: { get_param: start_stations }
            PARAM_EXOLABS_BASE_URL: { get_param: exolabs_base_url }
            PARAM_EXOLABS_API_KEY: { get_param: exolabs_api_key }
